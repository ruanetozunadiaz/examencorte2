const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){
    let celsius = parseFloat(document.getElementById('txtCantidad').value);
    let farenheit = parseFloat(document.getElementById('txtCantidad').value);

    let celsiusafarenheit = (9/5 * celsius) + 32;
    let farenheitacelsius = (farenheit - 32) * 5/9;

    let celsiustofarenheit = document.getElementById('celsius-farenheit').checked;
    let farenheittocelsius = document.getElementById('farenheit-celsius').checked;

    if(celsiustofarenheit){
        document.getElementById('txtResultado').value = celsiusafarenheit.toFixed(2);
    } else if (farenheittocelsius){
        document.getElementById('txtResultado').value = farenheitacelsius.toFixed(2);
    }
 });

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtCantidad').value = '';
    document.getElementById('txtResultado').value = '';
    document.getElementById('celsius-farenheit').checked = false;
    document.getElementById('farenheit-celsius').checked = false;
});
