const btnCalcular = document.getElementById('btnCalcular');
const valor = document.getElementById('opcionTipo');

btnCalcular.addEventListener('click', function(){
    const tipodeviaje = valor.value;

    if(tipodeviaje == "1"){
        let precio = parseFloat(document.getElementById('txtPrecio').value);
    
        document.getElementById('txtSubtotal').value = precio;

        let impuesto = precio * 0.16;

        document.getElementById('txtImpuesto').value = impuesto;

        let total = precio + impuesto;

        document.getElementById('txtTotal').value = total;
    } else if(tipodeviaje == "2"){
        let precio = parseFloat(document.getElementById('txtPrecio').value)
    
        let nuevoprecio = precio * 1.8;

        document.getElementById('txtSubtotal').value = nuevoprecio;

        let impuesto = nuevoprecio * 0.16;

        document.getElementById('txtImpuesto').value = impuesto;

        let total = nuevoprecio + impuesto;

        document.getElementById('txtTotal').value = total;
    }
});

const btnLimpiar = document.getElementById('btnLimpiar');

btnLimpiar.addEventListener('click', function () {
    document.getElementById('txtNumBoleto').value = '';
    document.getElementById('txtNombreCliente').value = '';
    document.getElementById('txtDestino').value = '';
    document.getElementById('opcionTipo').value = '1';
    document.getElementById('txtPrecio').value = '';
    document.getElementById('txtSubtotal').value = '';
    document.getElementById('txtImpuesto').value = '';
    document.getElementById('txtTotal').value = '';
});