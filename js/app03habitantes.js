const btnGenerar = document.getElementById('btnGenerar');
btnGenerar.addEventListener('click', function(){
    const edades = [];

    for (let x = 0; x < 100; x++) {
        edades.push(Math.floor(Math.random() * 100));
    }
    
    let bebes = 0;
    let ninos = 0;
    let adolescentes = 0;
    let adultos = 0;
    let ancianos = 0;
    let suma = 0;
    let promedio = 0;

    document.getElementById('txtEdades').textContent = edades.join(', ');

    for (let x = 0; x < edades.length; x++) {
        const edad = edades[x];
        if (edad >= 1 && edad <= 3) {
            bebes++;
        } else if (edad >= 4 && edad <= 12) {
            ninos++;
        } else if (edad >= 13 && edad <= 17) {
            adolescentes++;
        } else if (edad >= 18 && edad <= 60) {
            adultos++;
        } else if (edad >= 61 && edad <= 100) {
            ancianos++;
        }
        suma = suma + edad;
    }

    promedio = suma / 100;

    document.getElementById('txtBebes').value = bebes;
    document.getElementById('txtNiños').value = ninos;
    document.getElementById('txtAdolescentes').value = adolescentes;
    document.getElementById('txtAdultos').value = adultos;
    document.getElementById('txtAncianos').value = ancianos;
    document.getElementById('txtPromedio').value = promedio;
});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtEdades').textContent = '';
    document.getElementById('txtBebes').value = '';
    document.getElementById('txtNiños').value = '';
    document.getElementById('txtAdolescentes').value = '';
    document.getElementById('txtAdultos').value = '';
    document.getElementById('txtAncianos').value = '';
    document.getElementById('txtPromedio').value = '';
});


